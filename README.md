# danielspringl.cz

My personal website. Created with JavaScript framework Vue.js.

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```

### Vulnerability test
```
yarn audit
```

### Upgrade packages
```
yarn upgrade
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
