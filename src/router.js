import Vue from 'vue'
import Router from 'vue-router'
import Home from './components/Home.vue'
import Service from './components/Service.vue'
import Reference from './components/Reference.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/services',
      name: 'services',
      component: Service
    },
    {
      path: '/references',
      name: 'references',
      component: Reference
    },
    {
      path: '*',
      redirect: "/"
    },
  ]
})
